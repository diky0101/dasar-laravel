<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function kirim(Request $request){
        $firstname = $request->fname;
        $lastname = $request->lname;
        return view('selamat', compact('firstname', 'lastname'));
    }
}
