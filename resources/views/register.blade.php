<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Accounts Baru!</h1>
     <h3>Sign Up Form</h3>
     <form action="/selamat" method="POST">
        @csrf
         <label for="fname">First Name :</label><br>
         <br>
         <input type="text" name="fname" id="fname">
         <br><br>

         <label for="lname">Last Name :</label><br>
         <br>
         <input type="text" name="lname" id="lname">
         <br><br>

         <label for="gender">Gender :</label><br>
         <br>
         <input type="radio" name="gender" id="gender" value="male"><label for="male">Male</label><br>
         <input type="radio" name="gender" id="gender" value="female"><label for="female">Female</label><br>
         <input type="radio" name="gender" id="gender" value="others"><label for="others">Others</label><br>
         <br><br>
        <label for="nation">Nationality :</label><br>
        <br>
         <select name="nation" id="nation">
             <option value="">Pilih Nationality</option>
             <option value="1">Indonesia</option>
             <option value="2">Malaysia</option>
             <option value="3">Singapura</option>
             <option value="4">Others</option>
         </select>
         <br><br>

         <label for="">Language Spoken :</label><br><br>
         <input type="checkbox" name="bahasa" id="indo" value="indo"><label for="indo">Indonesia</label><br>
         <input type="checkbox" name="bahasa" id="eng" value="eng"><label for="eng">English</label><br>
         <input type="checkbox" name="bahasa" id="oth" value="oth"><label for="oth">Others</label><br>
         <br><br>
         <label for="bio">Bio :</label><br><br>
         <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
         <br>
         <button type="submit" name="signup">Sign Up</button>
     </form>
</body>
</html>